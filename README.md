# Rust Leptos Tailwind Sandbox

[![pipeline status](https://gitlab.com/alexforsale/rust-leptos-tailwind-sandbox/badges/main/pipeline.svg)](https://gitlab.com/alexforsale/rust-leptos-tailwind-sandbox/-/commits/main)

Leptos template using [Tailwindcss](https://tailwindcss.com/) + [flowbite](https://flowbite.com). See live at [https://alexforsale.gitlab.io/rust-leptos-tailwind-sandbox](https://alexforsale.gitlab.io/rust-leptos-tailwind-sandbox).
