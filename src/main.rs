use leptos::*;

pub mod app;
pub mod components;
use app::App;

pub fn main() {
    mount_to_body(|cx| {
        view! { cx, <App />}
    });
}
