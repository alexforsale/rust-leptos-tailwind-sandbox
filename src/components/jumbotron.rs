use leptos::*;

#[component]
pub fn Jumbotron(_cx: Scope) -> impl IntoView {
    let section_class = move || {
        "bg-white dark:bg-gray-900".to_string()
    };
    let first_div_class = move || {
        "py-8 px-4 mx-auto max-w-screen-xl text-center lg:py-16".to_string()
    };
    let h1_class = move || {
        "mb-4 text-4xl font-extrabold tracking-tight leading-none text-gray-900 md:text-5xl lg:text-6xl dark:text-white".to_string()
    };
    let p_class = move || {
        "mb-8 text-lg font-normal text-gray-500 lg:text-xl sm:px-16 lg:px-48 dark:text-gray-400".to_string()
    };
    let second_div_class = || {
        "flex flex-col space-y-4 sm:flex-row sm:justify-center sm:space-y-0 sm:space-x-4".to_string()
    };
    let first_button_class = || {
        "inline-flex justify-center items-center py-3 px-5 text-base font-medium text-center text-white rounded-lg bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 dark:focus:ring-blue-900".to_string()
    };
    let second_button_class = || {
        "inline-flex justify-center items-center py-3 px-5 text-base font-medium text-center text-gray-900 rounded-lg border border-gray-300 hover:bg-gray-100 focus:ring-4 focus:ring-gray-100 dark:text-white dark:border-gray-700 dark:hover:bg-gray-700 dark:focus:ring-gray-800".to_string()
    };

    view!{
        _cx,
        <section class=section_class>
            <div class=first_div_class>
                <h1 class=h1_class>"We invest in the world’s potential"</h1>
                <p class=p_class>"Here at Flowbite we focus on markets where technology, innovation, and capital can unlock long-term value and drive economic growth."</p>
                <div class=second_div_class>
                     <a href="#" class=first_button_class>
                         "Get Started"
                         <svg class="w-3.5 h-3.5 ml-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 10">
                             <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 5h12m0 0L9 1m4 4L9 9"/>
                         </svg>
                     </a>
                     <a href="#" class=second_button_class>
                         "Learn more"
                    </a>
                </div>
            </div>
        </section>
    }
}
