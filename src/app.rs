use leptos::*;
use leptos_meta::*;
use crate::components::jumbotron::Jumbotron;
use crate::components::navbar::NavBar;
use crate::components::banner::DefaultStickyBanner;
use crate::components::card::{CardWithImage, HorizontalCard, CardWithLink};
use crate::components::carousel::DefaultSliderCarousel;

#[component]
pub fn App(_cx: Scope) -> impl IntoView {
    provide_meta_context(_cx);
    let body_class = move || {
        "bg-white dark:bg-gray-900".to_string()
    };

    view! {
        _cx,
        <Meta charset="utf-8"/>
        <Meta http_equiv="X-UA-Compatible" content="IE=edge"/>
        <Meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <Body class=body_class />
        <NavBar />
        <DefaultStickyBanner />
        <Jumbotron />
        <div class="flex gap-4 justify-center">
            <CardWithImage />
            <HorizontalCard />
            <CardWithLink />
        </div>
        <DefaultSliderCarousel />
    }
}
